#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:20270414:98be27c7d60da3c1f4b430a2ae54e84b1fef44f6; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:19133770:ddd216abeb95da57f45e1e3cef424eb813c73ff2 EMMC:/dev/block/bootdevice/by-name/recovery 98be27c7d60da3c1f4b430a2ae54e84b1fef44f6 20270414 ddd216abeb95da57f45e1e3cef424eb813c73ff2:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
